import requests

from requests.models import Response
from rest_framework import status


def make_request(method, *args, **kwargs) -> [Response | None]:
    try:
        response = method(*args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            return None, status.HTTP_204_NO_CONTENT

        return response.json(), response.status_code

    except requests.exceptions.HTTPError as http_err:
        raise http_err

    except requests.exceptions.ConnectionError as conn_err:
        raise conn_err

    except Exception as err:
        raise err


def get(*args, **kwargs) -> Response:
    return make_request(requests.get, *args, **kwargs)


def post(*args, **kwargs) -> Response:
    return make_request(requests.post, *args, **kwargs)


def patch(*args, **kwargs) -> Response:
    return make_request(requests.patch, *args, **kwargs)


def delete(url, **kwargs) -> Response:
    return make_request(requests.delete, url, **kwargs)
