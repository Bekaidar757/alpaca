import base64
import json

import requests
from sseclient import SSEClient
from rest_framework import status

from .alpaca_services import AlpacaService
from orders.models import Order


class OrderService:
    def __init__(self):
        self.alpaca_service = AlpacaService()

    def create_order(self, account_id, **kwargs):
        try:
            alpaca_response, status_code = self.alpaca_service.create(
                account_id, **kwargs
            )

            if status_code == status.HTTP_200_OK:
                alpaca_response['account_id'] = account_id
                Order.objects.create(**alpaca_response)

            return alpaca_response, status_code

        except requests.exceptions.HTTPError as http_err:

            return (
                {"error": str(http_err)},
                status.HTTP_400_BAD_REQUEST
            )

        except requests.exceptions.ConnectionError as conn_err:

            return (
                {"error": str(conn_err)},
                status.HTTP_400_BAD_REQUEST
            )

        except Exception as err:

            return (
                {"error": str(err)},
                status.HTTP_400_BAD_REQUEST
            )

    def get_orders(self, account_id):
        try:
            return Order.objects.filter(account_id=account_id)

        except Exception:
            return Order.objects.none()

    def cancel_order(self, account_id, order_id):
        try:
            alpaca_response, status_code = self.alpaca_service.cancel(
                account_id, order_id
            )

            return alpaca_response, status_code

        except requests.exceptions.HTTPError as http_err:

            return (
                {"error": str(http_err)},
                status.HTTP_400_BAD_REQUEST
            )

        except requests.exceptions.ConnectionError as conn_err:

            return (
                {"error": str(conn_err)},
                status.HTTP_400_BAD_REQUEST
            )

        except Exception as err:

            return (
                {"error": str(err)},
                status.HTTP_400_BAD_REQUEST
            )

    def delete_order(self, order_id):
        try:
            order = Order.objects.get(id=order_id)
            order.delete()

        except Order.DoesNotExist:
            pass

    def update_order(self, order_id, status):
        try:
            order = Order.objects.get(id=order_id)
            order.status = status
            order.save()
        except Order.DoesNotExist:
            pass


class AlpacaSSEOrderService:
    order_service = OrderService()

    def __init__(self, api_key, secret_key):
        self.api_key = api_key
        self.secret_key = secret_key

    def get_credentials(self):
        credentials = f'{self.api_key}:{self.secret_key}'
        return base64.b64encode(credentials.encode('utf-8')).decode('utf-8')

    def connect(self, url):
        headers = {
            "Accept": "text/event-stream",
            "Authorization": f'Basic {self.get_credentials()}'
        }
        return SSEClient(url, headers=headers)

    def listen_events(self, url):
        while True:
            try:
                messages = self.connect(url)
                print("Successfully connected. Listening for events...")
                for msg in messages:
                    if msg.data:
                        data = json.loads(msg.data)
                        if data['event'] == 'canceled':
                            self.order_service.delete_order(
                                data['order']['id']
                            )
                        if data['event'] in [
                            'new', 'fill', 'partial_fill', 'expired',
                            'done_for_day', 'replaced'
                        ]:
                            self.order_service.update_order(
                                order_id=data['order']['id'],
                                status=data['order']['status']
                            )

            except Exception as e:
                print(f"An error occurred: {e}")
                print("Attempting to reconnect...")
