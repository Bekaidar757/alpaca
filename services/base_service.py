from abc import ABC
from .api_requests import get, post


class BaseService(ABC):
    BASE_URL = None

    def __init__(self, base_url):
        self.BASE_URL = base_url

    def list(self, basename, query_params=None):
        url = f'{self.BASE_URL}/{basename}?{query_params}'
        return get(url)

    def create(self, basename, data):
        url = f'{self.BASE_URL}/{basename}/'
        return post(url, data)
