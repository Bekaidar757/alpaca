import base64
import json
from decimal import Decimal

import requests

from core import settings
from .base_service import BaseService
from .api_requests import post, delete


class AlpacaService(BaseService):
    ALPACA_API_BASE_URL = "https://broker-api.sandbox.alpaca.markets"

    def __init__(self):
        super().__init__(self.ALPACA_API_BASE_URL)
        self.api_key = settings.ALPACA_API_KEY
        self.secret_key = settings.ALPACA_SECRET_KEY

    def create(self, account_id, **kwargs):
        url = f"{self.BASE_URL}/v1/trading/accounts/{account_id}/orders"
        credentials = f'{self.api_key}:{self.secret_key}'
        credentials_encode = base64.b64encode(
            credentials.encode('utf-8')
        ).decode('utf-8')

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": f'Basic {credentials_encode}'
        }

        try:
            for key, value in kwargs.items():
                if isinstance(value, Decimal):
                    kwargs[key] = str(value)

            json_data = json.dumps(kwargs)

            return post(url, json_data, headers=headers)

        except requests.exceptions.HTTPError as http_err:
            raise http_err

        except requests.exceptions.ConnectionError as conn_err:
            raise conn_err

        except Exception as err:
            raise err

    def cancel(self, account_id, order_id):
        url = (
            f"{self.BASE_URL}/v1/trading/accounts/{account_id}/"
            f"orders/{order_id}"
            )
        credentials = f'{self.api_key}:{self.secret_key}'
        credentials_encode = base64.b64encode(
            credentials.encode('utf-8')
        ).decode('utf-8')

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": f'Basic {credentials_encode}'
        }

        try:
            return delete(url, headers=headers)

        except requests.exceptions.HTTPError as http_err:
            raise http_err

        except requests.exceptions.ConnectionError as conn_err:
            raise conn_err

        except Exception as err:
            raise err
