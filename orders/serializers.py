from rest_framework import serializers

from .models import Order


class OrderCreateSerializer(serializers.ModelSerializer):
    """
        Включил только те поля которые обязательны,
        остальные поля по желанию,
        так как некоторые поля нельзя передовать вместе,
        срабатывает валидация на сервисе альпаки.
    """
    class Meta:
        model = Order
        fields = (
            'side', 'type', 'time_in_force', 'symbol', 'qty', 'notional'
        )


class OrderListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = '__all__'
