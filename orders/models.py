import uuid

from django.db import models


class Order(models.Model):
    SIDE_CHOICES = (
        ('buy', 'buy'),
        ('sell', 'sell'),
        ('buy_minus', 'buy_minus'),
        ('sell_plus', 'sell_plus'),
        ('sell_short', 'sell_short'),
        ('sell_short_exempt', 'sell_short_exempt'),
        ('undisclosed', 'undisclosed'),
        ('cross', 'cross'),
        ('cross_short', 'cross_short'),
    )
    TYPE_CHOICES = (
        ('market', 'market'),
        ('limit', 'limit'),
        ('stop', 'stop'),
        ('stop_limit', 'stop_limit'),
        ('trailing_stop', 'trailing_stop'),
    )
    TIME_IN_FORCE_CHOICES = (
        ('day', 'day'),
        ('gtc', 'gtc'),
        ('opg', 'opg'),
        ('cls', 'cls'),
        ('ioc', 'ioc'),
        ('fok', 'fok'),
    )
    ORDER_CLASS_CHOICES = (
        ('simple', 'simple'),
        ('bracket', 'bracket'),
        ('oco', 'oco'),
        ('oto', 'oto'),
    )
    account_id = models.CharField(max_length=48)
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False
    )
    client_order_id = models.UUIDField(
        unique=True, null=True, blank=True
    )
    created_at = models.DateTimeField(null=True, blank=True)
    updated_at = models.DateTimeField(null=True, blank=True)
    submitted_at = models.DateTimeField(null=True, blank=True)
    filled_at = models.DateTimeField(null=True, blank=True)
    expired_at = models.DateTimeField(null=True, blank=True)
    canceled_at = models.DateTimeField(null=True, blank=True)
    failed_at = models.DateTimeField(null=True, blank=True)
    replaced_at = models.DateTimeField(null=True, blank=True)
    asset_id = models.UUIDField(null=True, blank=True)
    symbol = models.CharField(max_length=50, default='AAPL')
    asset_class = models.CharField(max_length=50, null=True, blank=True)
    qty = models.DecimalField(
        max_digits=10, decimal_places=3, null=True, blank=True
    )
    notional = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True
    )
    filled_qty = models.DecimalField(
        max_digits=10, decimal_places=3, null=True, blank=True
    )
    filled_avg_price = models.DecimalField(
        max_digits=10, decimal_places=3, null=True, blank=True
    )
    order_type = models.CharField(max_length=50, null=True, blank=True)
    type = models.CharField(max_length=50, choices=TYPE_CHOICES)
    side = models.CharField(max_length=50, choices=SIDE_CHOICES)
    time_in_force = models.CharField(
        max_length=50, choices=TIME_IN_FORCE_CHOICES
    )
    limit_price = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True
    )
    stop_price = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True
    )
    status = models.CharField(max_length=50, null=True, blank=True)
    extended_hours = models.BooleanField(default=False)
    trail_price = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True
    )
    trail_percent = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True
    )
    order_class = models.CharField(
        max_length=50, choices=ORDER_CLASS_CHOICES, null=True, blank=True,
        default=''
    )
    subtag = models.CharField(max_length=50, null=True, blank=True)
    source = models.CharField(max_length=50, null=True, blank=True)
    replaced_by = models.CharField(max_length=50, null=True, blank=True)
    replaces = models.CharField(max_length=50, null=True, blank=True)
    legs = models.CharField(max_length=50, null=True, blank=True)
    hwm = models.CharField(max_length=50, null=True, blank=True)
    commission = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return f'Order - {self.id}'

    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'
        db_table = 'orders'
