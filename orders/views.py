from drf_spectacular.utils import extend_schema
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from .serializers import (
    OrderCreateSerializer, OrderListSerializer
)
from services.order_service import OrderService


class OrderCreateAPIView(GenericAPIView):
    serializer_class = OrderCreateSerializer
    order_service = OrderService()

    @extend_schema(
        tags=['Orders'],
        description="Создание ордера: Выставление нового ордера.",
        summary="Create Order",
        request=OrderCreateSerializer,
        responses={200: OrderListSerializer}
    )
    def post(self, request, account_id, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response, status_code = self.order_service.create_order(
            account_id, **serializer.validated_data
        )

        return Response(response, status=status_code)


class OrderListAPIView(GenericAPIView):
    serializer_class = OrderListSerializer
    order_service = OrderService()

    @extend_schema(
        tags=['Orders'],
        description="Вывод списка ордеров: Просмотр текущих ордеров.",
        summary="Get Orders",
        responses={200: OrderListSerializer}
    )
    def get(self, request, account_id):
        orders = self.order_service.get_orders(account_id)
        serializer = self.serializer_class(orders, many=True)
        return Response(serializer.data)


class OrderDeleteAPIView(GenericAPIView):
    order_service = OrderService()

    @extend_schema(
        tags=['Orders'],
        description="Отмена ордера: Процедура отмены ордера.",
        summary="Cancel Order",
        responses={204: None}
    )
    def delete(self, request, account_id, order_id):
        response, status_code = self.order_service.cancel_order(
            account_id, order_id
        )

        return Response(response, status=status_code)
