from django.urls import path

from .views import (
    OrderCreateAPIView, OrderListAPIView, OrderDeleteAPIView
)


urlpatterns = [
    path(
        'accounts/<uuid:account_id>/orders/create/',
        OrderCreateAPIView.as_view(), name='order-create'
    ),
    path(
        'accounts/<uuid:account_id>/orders/',
        OrderListAPIView.as_view(), name='order-list'
    ),
    path(
        'accounts/<uuid:account_id>/orders/<uuid:order_id>',
        OrderDeleteAPIView.as_view(), name='order-cancel'
    ),
]
