# Generated by Django 5.0.1 on 2024-01-28 18:19

import uuid
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('account_id', models.CharField(max_length=48)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('client_order_id', models.UUIDField(blank=True, null=True, unique=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('submitted_at', models.DateTimeField(blank=True, null=True)),
                ('filled_at', models.DateTimeField(blank=True, null=True)),
                ('expired_at', models.DateTimeField(blank=True, null=True)),
                ('canceled_at', models.DateTimeField(blank=True, null=True)),
                ('failed_at', models.DateTimeField(blank=True, null=True)),
                ('replaced_at', models.DateTimeField(blank=True, null=True)),
                ('asset_id', models.UUIDField(blank=True, null=True)),
                ('symbol', models.CharField(default='AAPL', max_length=50)),
                ('asset_class', models.CharField(blank=True, max_length=50, null=True)),
                ('qty', models.DecimalField(blank=True, decimal_places=3, max_digits=10, null=True)),
                ('notional', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('filled_qty', models.DecimalField(blank=True, decimal_places=3, max_digits=10, null=True)),
                ('filled_avg_price', models.DecimalField(blank=True, decimal_places=3, max_digits=10, null=True)),
                ('order_type', models.CharField(blank=True, max_length=50, null=True)),
                ('type', models.CharField(choices=[('market', 'market'), ('limit', 'limit'), ('stop', 'stop'), ('stop_limit', 'stop_limit'), ('trailing_stop', 'trailing_stop')], max_length=50)),
                ('side', models.CharField(choices=[('buy', 'buy'), ('sell', 'sell'), ('buy_minus', 'buy_minus'), ('sell_plus', 'sell_plus'), ('sell_short', 'sell_short'), ('sell_short_exempt', 'sell_short_exempt'), ('undisclosed', 'undisclosed'), ('cross', 'cross'), ('cross_short', 'cross_short')], max_length=50)),
                ('time_in_force', models.CharField(choices=[('day', 'day'), ('gtc', 'gtc'), ('opg', 'opg'), ('cls', 'cls'), ('ioc', 'ioc'), ('fok', 'fok')], max_length=50)),
                ('limit_price', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('stop_price', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('status', models.CharField(blank=True, max_length=50, null=True)),
                ('extended_hours', models.BooleanField(default=False)),
                ('trail_price', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('trail_percent', models.DecimalField(blank=True, decimal_places=2, max_digits=10, null=True)),
                ('order_class', models.CharField(blank=True, choices=[('simple', 'simple'), ('bracket', 'bracket'), ('oco', 'oco'), ('oto', 'oto')], default='', max_length=50, null=True)),
                ('subtag', models.CharField(blank=True, max_length=50, null=True)),
                ('source', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'verbose_name': 'Order',
                'verbose_name_plural': 'Orders',
                'db_table': 'orders',
            },
        ),
    ]
