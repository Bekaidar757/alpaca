from django.core.management.base import BaseCommand
from django.conf import settings

from services.order_service import AlpacaSSEOrderService


class Command(BaseCommand):
    help = 'Starts listening to Alpaca SSE events'

    url = 'https://broker-api.sandbox.alpaca.markets/v1/events/trades'

    def handle(self, *args, **options):
        api_key = settings.ALPACA_API_KEY
        secret_key = settings.ALPACA_SECRET_KEY

        alpaca_sse_client = AlpacaSSEOrderService(api_key, secret_key)
        alpaca_sse_client.listen_events(self.url)
