import pytest
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from orders.models import Order


ACCOUNT_ID = settings.BROKER_ID
TEST_ACCOUNT_ID = 'e94af3fb-ecd1-48cf-a286-04d6745c817e'
TEST_ORDER_ID = 'e94af3fb-ecd1-48cf-a286-04d6745c615b'


@pytest.fixture
def api_client():
    return APIClient()


@pytest.mark.django_db
def test_create_order_success(api_client):
    url = reverse('order-create', kwargs={'account_id': f'{ACCOUNT_ID}'})

    payload = {
        "side": "buy",
        "type": "market",
        "time_in_force": "day",
        "symbol": "AAPL",
        "notional": "1.00"
    }

    response = api_client.post(url, payload, format='json')

    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_create_order_failure_no_broker_id(api_client):
    url = reverse('order-create', kwargs={'account_id': f'{TEST_ACCOUNT_ID}'})

    payload = {
        "side": "buy",
        "type": "market",
        "time_in_force": "day",
        "symbol": "AAPL",
        "notional": "1"
    }

    response = api_client.post(url, payload, format='json')

    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_get_orders(api_client):
    payload = {
        "side": "buy",
        "type": "market",
        "time_in_force": "day",
        "symbol": "AAPL",
        "notional": "1"
    }
    Order.objects.create(account_id=TEST_ACCOUNT_ID, **payload)
    Order.objects.create(account_id=TEST_ACCOUNT_ID, **payload)

    url = reverse('order-list', kwargs={'account_id': TEST_ACCOUNT_ID})
    response = api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 2


@pytest.mark.django_db
def test_get_orders_failure(api_client):
    url = reverse('order-list', kwargs={'account_id': TEST_ACCOUNT_ID})
    response = api_client.get(url)

    assert response.status_code == status.HTTP_200_OK
    assert len(response.data) == 0


@pytest.mark.django_db
def test_cancel_order_success(api_client):
    order = Order.objects.create(
        account_id=ACCOUNT_ID,
        side="buy",
        type="market",
        time_in_force="day",
        symbol="AAPL",
        notional="1",
    )

    url = reverse(
        'order-cancel',
        kwargs={'account_id': order.account_id, 'order_id': order.id}
    )
    response = api_client.delete(url)

    assert response.status_code == status.HTTP_404_NOT_FOUND


@pytest.mark.django_db
def test_cancel_order_failure_not_found(api_client):
    invalid_order_id = TEST_ORDER_ID
    url = reverse(
        'order-cancel',
        kwargs={'account_id': TEST_ACCOUNT_ID, 'order_id': invalid_order_id}
    )

    response = api_client.delete(url)

    assert response.status_code == status.HTTP_401_UNAUTHORIZED
