from decimal import Decimal

import pytest
from django.conf import settings
from rest_framework.test import APIClient
from rest_framework import status

from services.order_service import OrderService
from orders.models import Order


ACCOUNT_ID = settings.BROKER_ID
TEST_ACCOUNT_ID = 'e94af3fb-ecd1-48cf-a286-04d6745c817e'
TEST_ORDER_ID = 'e94af3fb-ecd1-48cf-a286-04d6745c615b'


@pytest.fixture
def client():
    return APIClient()


@pytest.mark.django_db
def test_create_order_alpaca_success(client):
    payload = {
        "side": "buy",
        "type": "market",
        "time_in_force": "day",
        "symbol": "AAPL",
        "notional": "1"
    }

    response = client.post(f'/accounts/{ACCOUNT_ID}/orders/create/', payload)

    data = response.data

    assert data['side'] == payload['side']
    assert data['type'] == payload['type']
    assert data['time_in_force'] == payload['time_in_force']
    assert data['symbol'] == payload['symbol']
    assert data['notional'] == payload['notional']
    assert response.status_code == status.HTTP_200_OK


@pytest.mark.django_db
def test_create_order_alpaca_failure_no_broker_id(client):
    payload = {
        "side": "buy",
        "type": "market",
        "time_in_force": "day",
        "symbol": "AAPL",
        "notional": "1"
    }

    response = client.post(
        f'/accounts/{TEST_ACCOUNT_ID}/orders/create/', payload
    )

    assert response.status_code == status.HTTP_401_UNAUTHORIZED


@pytest.mark.django_db
def test_create_order_success():
    payload = {
        "account_id": TEST_ACCOUNT_ID,
        "side": "buy",
        "type": "market",
        "time_in_force": "day",
        "symbol": "AAPL",
        "notional": Decimal("1.00")
    }

    Order.objects.create(**payload)

    order = Order.objects.first()
    assert order.side == payload['side']
    assert order.symbol == payload['symbol']
    assert order.type == payload['type']
    assert order.time_in_force == payload['time_in_force']
    assert order.notional == payload['notional']


@pytest.mark.django_db
def test_create_order_failure_missing_required_fields(client):
    payload = {
        "type": "market",
        "time_in_force": "day",
        "symbol": "AAPL",
        "notional": Decimal("1.00")
    }

    response = client.post(
        f'/accounts/{TEST_ACCOUNT_ID}/orders/create/',
        data=payload, format='json'
    )

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@pytest.mark.django_db
def test_get_orders_with_orders():
    Order.objects.create(account_id=TEST_ACCOUNT_ID)

    service = OrderService()

    orders = service.get_orders(account_id=TEST_ACCOUNT_ID)

    assert orders.count() == 1
    assert orders.first().account_id == TEST_ACCOUNT_ID


@pytest.mark.django_db
def test_get_orders_without_orders():
    non_existing_account_id = TEST_ACCOUNT_ID

    service = OrderService()

    orders = service.get_orders(account_id=non_existing_account_id)

    assert len(orders) == 0


@pytest.mark.django_db
def test_delete_existing_order():
    order = Order.objects.create(
        account_id=TEST_ACCOUNT_ID,
        side="buy",
        type="market",
        time_in_force="day",
        symbol="AAPL",
        notional="1",
    )

    service = OrderService()

    service.delete_order(order_id=order.id)

    with pytest.raises(Order.DoesNotExist):
        Order.objects.get(id=order.id)


@pytest.mark.django_db
def test_delete_non_existing_order():
    service = OrderService()

    service.delete_order(order_id=TEST_ORDER_ID)

    assert Order.objects.count() == 0


@pytest.mark.django_db
def test_update_order_success():
    initial_status = "accepted"
    Order.objects.create(id=TEST_ORDER_ID, status=initial_status)

    new_status = "new"
    service = OrderService()
    service.update_order(TEST_ORDER_ID, new_status)

    updated_order = Order.objects.get(id=TEST_ORDER_ID)
    assert updated_order.status == new_status
