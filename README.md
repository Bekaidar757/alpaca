# Веб-приложение для управления ордерами через Alpaca API

Этот проект представляет собой веб-приложение, разработанное для управления ордерами через Alpaca API.  
Приложение позволяет пользователям создавать, просматривать и управлять ордерами на бирже с использованием API от Alpaca.


### Запуск приложения

1. Склонируйте репозиторий на свой локальный компьютер:
https://gitlab.com/Bekaidar757/alpaca.git
2. Перейдите в каталог проекта:  
cd alpaca
3. Вручную:  
   - Создайте файл .env в корне вашего проекта.
   - Откройте файл .env в текстовом редакторе.
     - Добавьте секретные данные в формате KEY=VALUE. Например:  
    SECRET_KEY=your_secret_key  
    DEBUG=your_debug  
    DB_NAME=your_db_name  
    DB_USER=your_db_user  
    DB_PASSWORD=your_db_password  
    DB_HOST=db(Как указано в docker-compose.yml)  
    DB_PORT=your_db_port  
    ALPACA_API_KEY=your_alpaca_api_key  
    ALPACA_SECRET_KEY=your_alpaca_secret_key  
    BROKER_ID=your_broker_id(Обязательно для тестов)
4. Запустите проект с помощью Docker Compose:  
docker-compose up -d

### Локальное тестирование
Для тестирования приложения локально, выполните следующие шаги:  
1. Запуск линтера  
Перед выполнением тестов рекомендуется запустить линтер для проверки стиля кода. В корне проекта выполните команду:  
   - flake8 .
2. Запуск тестов pytest
Для запуска тестов с использованием pytest, выполните следующую команду:
   - pytest

### Настройка CI/CD Pipeline
В проекте настроен CI/CD Pipeline для автоматической проверки кода и запуска тестов при каждом push в ветку main.  
CI/CD Pipeline включает в себя запуск линтера и тестов для обеспечения качества кода и функциональности приложения.

Если вы хотите запустить тесты и линтер локально перед пушем в репозиторий, убедитесь, что они проходят без ошибок.  
Это поможет избежать некорректных изменений в вашем коде и ускорит процесс CI/CD Pipeline.

Swagger UI предоставляет удобный интерфейс для ознакомления с вашим API,  
включая описание эндпоинтов, параметров запросов и ответов.

### Документация API с помощью Swagger UI
Вы можете использовать Swagger UI для автодокументирования вашего API.  
Для просмотра документации перейдите по следующему URL-адресу в вашем браузере:  

http://localhost:8000/api/schema/swagger-ui/

### Эндпоинты API
#### Просмотр текущих ордеров:
Этот эндпоинт позволяет просматривать текущие ордеры на бирже для определенного аккаунта.  
**Метод:** GET  
**URL:** http://localhost:8000/accounts/{account_id}/orders/  
**Пример запроса:**  
```curl -X GET http://localhost:8000/accounts/199af291-fc28-4203-82ac-a6fbc63434f1/orders/```  
**Пример ответа:**  
```angular2html
[
    {
        "id": "199af291-fc28-4203-82ac-a6fbc63434f1",
        "account_id": "199af291-fc28-4203-82ac-a6fbc63434f1",
        "symbol": "AAPL",
        "side": "buy",
        "type": "market",
        "notional": "1",
        "status": "accepted",
        ...
    },
    {
        "id": "199af291-fc28-4203-82ac-a6fbc63434f1",
        "account_id": "199af291-fc28-4203-82ac-a6fbc63434f1",
        "symbol": "AAPL",
        "side": "sell",
        "type": "market",
        "qty": "1.3",
        "status": "new",
        ...
    },
    ...
]
```
#### Создание нового ордера:
Этот эндпоинт позволяет создать новый ордер на бирже для определенного аккаунта.  
**Метод:** POST  
**URL:** http://localhost:8000/accounts/{account_id}/orders/create/  
**Пример запроса:**
```angular2html
curl -X 'POST' \
  'http://localhost:8000/accounts/199af291-fc28-4203-82ac-a6fbc63434f1/orders/create/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "side": "buy",
  "type": "market",
  "time_in_force": "day",
  "symbol": "AAPL",
  "notional": "1"
}'
```
**Пример ответа:**
```angular2html
{
    "id": "199af291-fc28-4203-82ac-a6fbc63434f1",
    "account_id": "199af291-fc28-4203-82ac-a6fbc63434f1",
    "symbol": "AAPL",
    "side": "sell",
    "type": "market",
    "qty": "1.3",
    "status": "accepted",
    ...
}
```
#### Отмена ордера:
Этот эндпоинт позволяет отменить определенный ордер на бирже для определенного аккаунта.  
**Метод:** DELETE  
**URL:** http://localhost:8000/accounts/{account_id}/orders/{order_id}  
**Пример запроса:**
```
curl -X DELETE http://localhost:8000/accounts/199af291-fc28-4203-82ac-a6fbc63434f1/orders/949ff148-32f2-42b7-b1df-f3cd40add55d
```
**Пример ответа:**
```
HTTP/1.1 204 No Content
```
### Контактная информация
Электронная почта: beka1990.30.10@gmail.com  
Telegram: bekaidar7